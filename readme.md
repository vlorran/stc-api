# Laravel 5.2 Api
Brayan asked me to create a Laravel API instead of using the one API of the description of the problem.

## Description
I made a simple contact list using OAUTH2. This API can create an user, add a contact, get contacts, get messages and get the user's data.

### Creating an user
To create an user you simple need to send the user's data (email, phone, name and password) to http://stc.datayet.com/register via POST.

### Authenticate User
To Authenticate an user you'll need to send the username(email), password, client_id and client_secret to http://stc.datayet.com/oauth/access_token via POST. it will return the access token to use next.

### Add a contact
To add a contact you just need to send a existing number and the access token to http://stc.datayet.com/add-contact via POST.

### Get contacts
To get all contacts of an user you just need make a GET request to http://stc.datayet.com/contacts passing the access token.

### Get messages
To get all messages of a conversation just make a GET request to http://stc.datayet.com/messages passing the access token and the id of the user that you want the messages.

### Get user data
To retrive all data of the current users just make a GET request to http://stc.datayet.com/my-data passing the access token.
