<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/teste', function(){
	return 'asdas';
});

Route::post('oauth/access_token', function() {
    return Response::json(Authorizer::issueAccessToken());
});

Route::group(['middleware' => 'cors'], function(Illuminate\Routing\Router $router){
	
	$router->post('register', 'UserController@register');

	$router->group(['middleware' => 'oauth'], function(Illuminate\Routing\Router $router){
		$router->post('add-contact',  'ApiController@add_contact');
		$router->post('add-message',  'ApiController@add_message');
		$router->get('contacts',  'ApiController@get_contact');
		$router->get('messages',  'ApiController@get_messages');
		
		$router->get('my-data',  'ApiController@get_my_data');
	});
});
