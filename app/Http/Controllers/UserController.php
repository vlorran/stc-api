<?php

namespace App\Http\Controllers;

use App\User;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller{

	public function register(Request $request){
		$validator = Validator::make($request->all(), [
	        'email' => 'required|unique:users|email',
	        'name' => 'required',
	        'phone' => 'required|numeric|unique:users',
	        'password' => 'required|max:3',
	    ]);
	    if($validator->fails()){
	    	return response()->json(['errors'=>$validator->errors()->all()]);
	    }
	    $user = new User();
	    $user->email = $request->email;
	    $user->name = $request->name;
	    $user->phone = $request->phone;
	    $user->password = \Illuminate\Support\Facades\Hash::make($request->password);
	    $user->save();
	    return response()->json(['saved'=>true]);
	}
}
