<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use App\Contact;
use App\Message;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use LucaDegasperi\OAuth2Server\Facades\Authorizer;

class ApiController extends Controller{

	public function add_contact(Request $request){
		$user_id = Authorizer::getResourceOwnerId();
		$current_user = User::find($user_id);
		$user = User::wherePhone($request->number)->where('id','<>',$user_id)->first();
		if($user){
			$contact = $current_user->contacts()->whereContactUserId($user->id)->first();
			if(!$contact){
				$current_user->contacts()->save(new Contact(['contact_user_id'=>$user->id]));
				$user->contacts()->save(new Contact(['contact_user_id'=>$user_id]));
			}
			return response()->json([
				'id'=>$user->id,
				'name'=>$user->name,
				'email'=>$user->email,
				'phone'=>$user->phone,
			]);
		}
		return response()->json([
			'error'=>'User not found.'
		]);
	}

	public function add_message(Request $request){
		$user_id = Authorizer::getResourceOwnerId();
		$user = User::whereId($request->user_id)->where('id','<>',$user_id)->first();
		if($user){
			$message = new Message([
				'from_user_id'=>$user_id,
				'to_user_id'=>$user->id,
				'message'=>$request->message
			]);
			$message->save();
			return response()->json([
				'sent'=>true,
				'message'=>$message
			]);
		}
		return response()->json([
			'error'=>'User not found.'
		]);
	}

	public function get_contact(Request $request){
		$user_id = Authorizer::getResourceOwnerId();
		$current_user = User::find($user_id);
		return response()->json(['users'=>$current_user->contacts_users()->get()]);
	}

	public function get_messages(Request $request){
		$user_id = Authorizer::getResourceOwnerId();
		$current_user = User::find($user_id);
		$messages = Message::whereRaw("(from_user_id = $user_id AND to_user_id = {$request->user_id}) OR (to_user_id = $user_id AND from_user_id = {$request->user_id})")
		->orderBy('created_at', 'asc')
		->get();
		return response()->json(['messages'=>$messages,'my_id'=>$user_id]);
	}

	public function get_my_data(Request $request){
		$user_id = Authorizer::getResourceOwnerId();
		$current_user = User::find($user_id);
		return response()->json(['user'=>$current_user]);
	}
}
