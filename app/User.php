<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'pivot'
    ];

    public function contacts(){
        return $this->hasMany('App\Contact','user_id');
    }

    public function contacts_users(){
        return $this->belongsToMany('App\User', 'contacts', 'user_id', 'contact_user_id');
    }

}
